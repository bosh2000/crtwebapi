﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CrtWebApi.Controllers.Image;
using Newtonsoft.Json;

namespace CrtWebApi.Controllers
{
    public class GetStatusController : ApiController
    {
        [HttpGet]
        public string GetStatus(int id)
        {
            var response = CoreProcessImage.GetStatusProcess(id);

            return JsonConvert.SerializeObject(response);
        }
    }
}
