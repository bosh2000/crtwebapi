﻿using CrtWebApi.Controllers.Types;
using ImageProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace CrtWebApi.Controllers.Image
{
    public static class CoreProcessImage
    {
        private static int _counter = 0;
        private static List<ProcessList> _processLists = new List<ProcessList>();

        public static Response UploadImage(HttpRequestMessage request)
        {
            ProcessList process = new ProcessList();
            Response response = new Response();
            try
            {
                if (!request.Content.IsMimeMultipartContent("form-data"))
                {
                    throw new HttpResponseException(request.CreateResponse(HttpStatusCode.UnsupportedMediaType));
                }

                string root = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Uploads");
                MultipartFormDataStreamProvider streamProvider = new MultipartFormDataStreamProvider(root);

                request.Content.ReadAsMultipartAsync(streamProvider);
                process.fileName = streamProvider.FileData.First().LocalFileName;
                process.thread = CreateThreadProcessImage(process.fileName);
                process.Id = _counter++;
                _processLists.Add(process);
                response.ProcessId = process.Id;
                response.Status = true;
            }
            catch (Exception exp)
            {
                response.ProcessId = int.MinValue;
                response.Status = false;
            }

            return response;
        }

        public static Thread CreateThreadProcessImage(string fileName)
        {
            Thread thread = new Thread(new ParameterizedThreadStart(ThreadProcessImage));
            thread.Start(fileName);
            return thread;
        }

        public static void ThreadProcessImage(object fileName)
        {
            PngProcessor pngProcessor = new PngProcessor();
            pngProcessor.Process((string)fileName);
        }

        public static Response GetStatusProcess(int id)
        {
            Response response = new Response()
            {
                ProcessId = id
            };

            ProcessList process = _processLists.Find((x) => x.Id == id);

            if (process != null && process.thread.IsAlive)
            {
                response.Status = true;
            }
            else
            {
                response.Status = false;
            }

            return response;
        }

        public static Response StopProcess(int id)
        {
            Response response = new Response();

            ProcessList process = _processLists.Find((x) => x.Id == id);

            if (process != null)
            {
                process.thread.Abort();
                response.Status = true;
            }
            else
            {
                response.Status = false;
            }

            return response;
        }
    }
}