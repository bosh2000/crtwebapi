﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CrtWebApi.Controllers.Image;
using Newtonsoft.Json;

namespace CrtWebApi.Controllers
{
    public class StopProcessController : ApiController
    {
        [HttpDelete]
        public string StopProcess(int id)
        {
            var response = CoreProcessImage.StopProcess(id);

            return JsonConvert.SerializeObject(response);
        }
    }
}
