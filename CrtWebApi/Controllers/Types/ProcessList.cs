﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace CrtWebApi.Controllers.Types
{
    public class ProcessList
    {
        public Thread thread { get; set; }
        public string fileName { get; set; }
        public int Id { get; set; }
    }
}