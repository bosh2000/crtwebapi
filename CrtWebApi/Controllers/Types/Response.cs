﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrtWebApi.Controllers.Types
{
    public class Response
    {
        public bool Status { get; set; }
        public int ProcessId { get; set; }

    }
}