﻿using CrtWebApi.Controllers.Image;
using CrtWebApi.Controllers.Types;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;

namespace CrtWebApi.Controllers
{
    public class UploadController : ApiController
    {
        [HttpPost]
        public string CrtUpload()
        {
            var response = CoreProcessImage.UploadImage(Request);

            return JsonConvert.SerializeObject(response);
       
        }
    }
}